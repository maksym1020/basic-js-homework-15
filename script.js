// Практичне завдання 1:
// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди.
// Новий текст повинен вказувати, що операція виконана успішно.


const div = document.querySelector('#root');
const timerButton = document.querySelector('.timer-button');

timerButton.addEventListener('click', ()=>{
    setTimeout(changeText, 3000);
})

function changeText(){
    const successText = document.createElement('p');

    successText.textContent = 'Операція виконана успішно'

    div.append(successText);
}


// Практичне завдання 2
// Реалізуйте таймер зворотного відліку, використовуючи setInterval.
// При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
// Після досягнення 1 змініть текст на "Зворотній відлік завершено".

let i = 10;

let counterElement = document.createElement('p');
div.append(counterElement);

window.addEventListener('load', () => {

    let countDown = setInterval(onLoadDisplayCounter, 1000);
    function onLoadDisplayCounter() {

        counterElement.innerText = `${i--}`;

        if (counterElement.innerText === '0'){
            counterElement.innerText = 'Зворотній відлік завершено';
            clearInterval(countDown);
        }
    }
});


